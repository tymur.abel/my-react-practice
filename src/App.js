import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { itemManagerActions } from './store';
import List from './components/List/List';
import Control from './components/Control/Control';

const App = () => {
  const items = useSelector((state) => state.items);
  const dispatch = useDispatch();

  useEffect(() => {
    const storedList = localStorage.getItem('my_list');
    if (storedList !== null) {
      const list = JSON.parse(storedList);
      dispatch(itemManagerActions.setItems(list));
    }
  }, []);

  const addItemHandler = (item) => {
    dispatch(itemManagerActions.addItem(item));
  };
  const removeItemHandler = (name) => {
    dispatch(itemManagerActions.removeItem(name));
  };

  return (
    <>
      <h1 className="text-center">My List</h1>
      <List items={items} onRemoveItem={removeItemHandler} />
      <br />
      <Control onAddItem={addItemHandler} />
    </>
  );
};

export default App;
