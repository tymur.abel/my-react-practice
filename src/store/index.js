import { configureStore, createSlice } from '@reduxjs/toolkit';

const manageListItems = createSlice({
  name: 'itemManager',
  initialState: { items: [] },
  reducers: {
    setItems(state, action) {
      state.items.splice(0);
      action.payload.forEach((e) => state.items.push(e));
    },
    addItem(state, action) {
      const item = action.payload;
      if (item == null) return;
      const existingItem = state.items.find((elem) => elem.name === item.name);
      if (!existingItem) state.items.push(item);
      else existingItem.amount += item.amount;
      localStorage.setItem('my_list', JSON.stringify(state.items));
    },
    removeItem(state, action) {
      const newItems = state.items.filter((item) => item.name !== action.payload);
      state.items.splice(0);
      newItems.forEach((e) => state.items.push(e));
      localStorage.setItem('my_list', JSON.stringify(newItems));
    }
  }
});

const store = configureStore({
  reducer: manageListItems.reducer
});

export const itemManagerActions = manageListItems.actions;

export default store;
