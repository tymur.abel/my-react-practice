import PropTypes from 'prop-types';

const ListItem = ({ name, amount, onRemoveItem }) => {
  const removeItemHandler = () => {
    onRemoveItem(name);
  };

  return (
    <li>
      <h2 className="inline half-width">{name}</h2>
      <h2 className="inline text-right">{amount}</h2>
      <button type="button" onClick={removeItemHandler}>X</button>
    </li>
  );
};

ListItem.propTypes = {
  name: PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired,
  onRemoveItem: PropTypes.func.isRequired
};

export default ListItem;
