import PropTypes from 'prop-types';
import ListItem from './ListItem';

const List = ({ items, onRemoveItem }) => {
  let listDom = <p className="text-center">This list is empty</p>;
  
  if (items && items.length > 0) {
    listDom = (
      <ul className="list-card m-auto">
        {items.map((item) => (
          <ListItem
            key={item.id}
            name={item.name}
            amount={item.amount}
            onRemoveItem={onRemoveItem}
          />
        ))}
      </ul>
    );
  }

  return listDom;
};

List.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onRemoveItem: PropTypes.func.isRequired
};

export default List;
