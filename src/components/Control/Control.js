import PropTypes from 'prop-types';
import { useState, useRef } from 'react';

const Control = ({ onAddItem }) => {
  const [controlsVisible, setVisible] = useState(false);
  const [nameInputValid, setNameInputValid] = useState(true);
  const [amtInputValid, setAmtInputValid] = useState(true);
  const itemNameRef = useRef();
  const itemAmtRef = useRef();

  const showControlsHandler = () => { setVisible(true); };
  const hideControlsHandler = (event) => {
    event.preventDefault();
    setVisible(false);
  };
  const Validate = (str0, str1) => {
    if (str0 === '') {
      setNameInputValid(false);
      return null;
    }
    if (str1 === '') {
      setAmtInputValid(false);
      return null;
    }
    if (+str1 <= 0) {
      setAmtInputValid(false);
      return null;
    }
    setNameInputValid(true);
    setAmtInputValid(true);
    return { name: str0, amount: +str1 };
  };
  const submitFormHandler = (event) => {
    event.preventDefault();
    const value = Validate(itemNameRef.current.value, itemAmtRef.current.value);
    if (value !== null) {
      onAddItem({
        id: Math.random().toString(),
        name: value.name,
        amount: value.amount,
      });
      setVisible(false);
    }
  };

  return (
    <div className={`${controlsVisible ? 'list-card' : ' center-objects'}`}>
      {!controlsVisible && <button type="button" onClick={showControlsHandler}>Add Item</button>}
      {controlsVisible && (
      <form onSubmit={submitFormHandler}>
        <div>
          <p className="inline half-width">Name</p>
          <input
            ref={itemNameRef}
            type="text"
            className={`inline half-width ${!nameInputValid ? 'invalid' : ''}`}
          />
        </div>
        <div>
          <p className="inline half-width">Amount</p>
          <input
            ref={itemAmtRef}
            type="number"
            className={`inline half-width ${!amtInputValid ? 'invalid' : ''}`}
          />
        </div>
        <button type="submit">Add</button>
        <button type="button" onClick={hideControlsHandler}>Cancel</button>
      </form>
      )}
    </div>
  );
};

Control.propTypes = {
  onAddItem: PropTypes.func.isRequired
};

export default Control;
